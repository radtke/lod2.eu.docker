FROM ubuntu:14.10

MAINTAINER Natanael Arndt <arndt@informatik.uni-leipzig.de>

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# update ubuntu trusty
RUN echo "deb http://archive.ubuntu.com/ubuntu trusty main universe" > /etc/apt/sources.list
RUN apt-get update

# install some basic packages
# install the nginx server with PHP
# install virtuoso
RUN apt-get install -y \
    git make curl \
    nginx-light php5-fpm php5-odbc php5-curl \
    virtuoso-opensource raptor-utils

# clone ontowiki and get its dependencies
RUN git clone https://github.com/AKSW/OntoWiki.git /var/www/
WORKDIR /var/www/
RUN git checkout deployment/lod2.eu
RUN make deploy
ADD config.ini /var/www/config.ini
#RUN cp config.ini.dist config.ini

WORKDIR /
# configure the ontowiki site for nginx
ADD ontowiki.conf /etc/nginx/sites-available/
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/ontowiki.conf /etc/nginx/sites-enabled/

# configure odbc for virtuoso
ADD odbc.ini /etc/

ADD id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
ADD known_hosts /root/.ssh/known_hosts

ADD virtuoso.ini.patch /virtuoso.ini.patch
RUN patch /etc/virtuoso-opensource-6.1/virtuoso.ini < virtuoso.ini.patch
ADD dump_one_graph.virtuoso.sql /dump_one_graph.virtuoso.sql

# Add startscript and start
ADD start.sh /start.sh
ADD ow-docker.fig /ow-docker.fig
CMD ["/bin/bash", "/start.sh"]

# expose the HTTP port to the outer world
EXPOSE 80
