#!/bin/sh

VOS_PASSWORD="jage172dehn"

# start the virtuoso service
echo "starting virtuoso …"
service virtuoso-opensource-6.1 start

# start the php5-fpm service
echo "starting php …"
service php5-fpm start

# start the nginx service
echo "starting nginx …"
service nginx start

echo "set virtuoso password …"

echo "user_set_password('dba', '${VOS_PASSWORD}');" | isql-vt 1111 dba dba

echo "define dump_one_graph …"
cat dump_one_graph.virtuoso.sql | isql-vt 1111 dba ${VOS_PASSWORD}

echo "loading data into database …"
git clone ifigit@git.informatik.uni-leipzig.de:radtke/lod2.eu.models.git models
cd models
./import.sh ${VOS_PASSWORD}


echo "setup git for user …"
git config user.email "backup@lod2.eu"
git config user.name "Backup Bot"
git config push.default matching

echo "setup backup cronjob …"
echo "0 * * * * cd /models/ && ./backup.sh ${VOS_PASSWORD} commit >/dev/null 2>/dev/null" | crontab
cron

echo "OntoWiki is ready to set sail!"
cat /ow-docker.fig
echo ""
echo "following log:"

OWLOG="/var/www/logs/ontowiki.log"
touch $OWLOG
chmod a+w $OWLOG
tail -f $OWLOG
